var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.bicicleta_list);
//ruta para el get de los datos 
router.get('/create',bicicletaController.bicicleta_create_get);
//ruta para el post de los datos obtenidos, ojo con el .post
router.post('/create',bicicletaController.bicicleta_create_post);
//ruta para el show de los datos 
router.get('/:id/show',bicicletaController.bicicleta_show_get);
//ruta para el post de los datos obtenidos, ojo con el .post
router.post('/:id/show',bicicletaController.bicicleta_show_post);
//ruta para el update de los datos 
router.get('/:id/update',bicicletaController.bicicleta_update_get);
//ruta para el post de los datos obtenidos, ojo con el .post
router.post('/:id/update',bicicletaController.bicicleta_update_post);
//ruta para el delete del id de bicicleta
router.post('/:id/delete',bicicletaController.bicicleta_delete_post);

//exporto el router luego de generarlo
module.exports = router;