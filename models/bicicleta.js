var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function (){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else 
        throw new Error(`No existe una bicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
    //verificamos que el id exista 
    Bicicleta.findById(aBiciId);
    //recorremos el arreglo de bicicletas para encontrar el id a eliminar
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            // eliminamos el id
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

// var a = new Bicicleta(1,'rojo','urbana',[-34.606323, -58.434965])
// var b = new Bicicleta(2,'blanca','urbana',[-34.6088146,-58.4306921])

// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = Bicicleta;